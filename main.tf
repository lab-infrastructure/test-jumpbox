terraform {
  backend "http" {
  }
}

module "test_jumpbox" {
    #source = "../xoa-dynamic-instance"
    source = "gitlab.com/lab-infrastructure/xoa-dynamic-instance/local"
    version = "0.0.8"

    realm = "core.dylanlab.xyz"
    vm_name_prefix = "test-jumpbox-"
    vm_name_suffix = "core-dhcp"
    vm_name_description = "test-jumpbox"
    vm_count = 2 
    username_admin = var.username_admin
    public_key_admin = var.public_key_admin
    username_ansible = var.username_ansible
    public_key_ansible = var.public_key_ansible
    xen_pool_name = "xcp-ng-pool-01"
    xen_template_name = "ubuntu-focal-20.04-cloudimg-20220124"
    xen_storage_name = "iscsi-vm-store"
    xen_network_name = "core.dylanlab.xyz"
    vm_wait_for_ip = true
    vm_tags = [
        "ansible",
        "ubuntu20.04"
    ]

    #vm_disk_size_gb = ""
    #vm_memory_size_gb = ""
    #vm_cpu_count = ""
}

output "dynamic_hostnames" {
    value = module.test_jumpbox.instance_hostnames
}

output "dynamic_ipv4" {
    value = module.test_jumpbox.instance_ipv4_address_first
}

resource "local_file" "rke_config" {
  filename = "Test.yml"
  content = yamlencode({
    nodes : concat([for k,e in module.test_jumpbox.instance_hostnames[*] : {
        address : e,
        internal_address : module.test_jumpbox.instance_ipv4_address_first[k],
        port : 22,
        role : 4,
        user : "ff",
        labels : "ff"
    }
    ])
    })

}